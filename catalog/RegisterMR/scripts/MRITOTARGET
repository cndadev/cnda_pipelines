#!/bin/bash

# The purpose of this script is to transform a T1 weighted MPRAGE MRI
# into 711-2B 222 atlas target space for ROI determination used in PET processing.

VERSION=2.2
AUTHOR="flavin, jon c"
DATE=20150116
if [ "$1" == "--version" ]; then
    echo $VERSION
    exit 0
fi

die(){
    echo >&2 "$@"
    exit 1
}

program=`basename $0`
idstr='$Id: '$program',v '$VERSION' '$DATE' '$AUTHOR' $'
echo $idstr


if [ $# -lt 2 ]; then
    echo "Arg Value must be 2 Must exit";
    echo "Usage: "
    echo "$program MRI.4dfp.img targetAtlas";
    echo "MRI may be T1 weighted 4dfp"
    echo "MRI_on_711-2B.4dfp.img is output"
    exit 1;
fi

mrmprfstr=$1    # Anatomical MR file (T1.mgz, orig.mgz, or T1.4dfp.img)
target=$2   # 711-2B.4dfp.img must exist in $targetpath
targetpath=$REFDIR   # /data/petsun43/data1/atlas

mrname=`basename $mrmprfstr`
mrdir=`dirname $mrmprfstr`
mrroot=${mrname%.*}
mrroot=${mrroot%.*}

space=$target   # 711-2B

###################################################################################################
# Set up MRI file

if [ -e $mrdir/${mrroot}".4dfp.img" ] ; then
    echo ${mrroot}".4dfp.img" exists
else
    echo "============================================================="
    echo Attempting mgz conversion to 4dfp file
    echo
    mgzto4dfp $mrmprfstr || die ${program}:mgzto4dfp $mrmprfstr "failed Must exit"
fi

###################################################################################################
# PROCESS MR

if [ ! -e ${mrroot}"_to_"${target}"_t4" ]
then
    # register mr to atlas
    mpr2atl_4dfp $mrdir/$mrroot -T$targetpath/$target -S$space || die ${program}:"mpr2atl_4dfp failed Must exit"
fi

if [ ! -e ${mrroot}"_mskt.4dfp.img" ]; then
    # generate brainmask
    msktgen_4dfp $mrroot -T$targetpath/$target -S$space || die "$program ERROR: brainmask generation failed"
fi

###################################################################################################

echo "============================================================="
echo Resample MR to target 222
echo
t4img_4dfp $mrroot"_to_"${target}"_t4" $mrroot ${mrroot}"_on_"${target} -O222 || die ${program}:"$mrroot transform failed Must exit"

echo removing ${mrroot}"_g11"
rm ${mrroot}"_g11.4dfp"*

echo "$program Done"

exit 0
