#!/bin/bash -e

if [ "$1" == "--version" ]; then
    echo 1
    exit 0
fi

die(){
    echo >&2 "$@"
    exit 1
}

mouse=$1
date=$2
system=$3
datadir=$4
framerate=$5
rs_freqout=$6
rs_lowpass=$7
rs_highpass=$8
stim_freqout=$9
stim_lowpass=${10}
stim_highpass=${11}
stim_blocksize=${12}
stim_baseline=${13}
stim_duration=${14}
has_fc=${15}
has_stim=${16}

dir=`pwd`

echo "Running OIS processing"
if [ $has_fc == true ]; then
    echo "Processing fc scans"

    matlab -nosplash -nodisplay << END || die "Processing fc failed"
addpath(genpath('/data/CNDA/pipeline/catalog/ois/resources/matlab'));

info.framerate=str2num('$framerate');

suffix='fc';
info.freqout=str2num('$rs_freqout');
info.lowpass=str2num('$rs_lowpass');
info.highpass=str2num('$rs_highpass');

ProcMultiOISFiles('$date', '$mouse', suffix, '$dir/', '$datadir/', info, '$system');
OISQC('$date', '$mouse', suffix, '$dir/', '$datadir/', info, '$system');
TransformDatahb('$date', '$mouse', suffix);
exit;
END

    echo "Done"
    echo
fi

if [ $has_stim == true ]; then
    echo "Processing stim scans"

    matlab -nosplash -nodisplay << END || die "Processing stim failed"
addpath(genpath('/data/CNDA/pipeline/catalog/ois/resources/matlab'));

info.framerate=str2num('$framerate');

suffix='stim';
info.freqout=str2num('$stim_freqout');
info.lowpass=str2num('$stim_lowpass');
info.highpass=str2num('$stim_highpass');
info.stimblocksize=str2num('$stim_blocksize');
info.stimbaseline=str2num('$stim_baseline');
info.stimduration=str2num('$stim_duration');

ProcMultiOISFiles('$date', '$mouse', suffix, '$dir/', '$datadir/', info, '$system');
OISQC('$date', '$mouse', suffix, '$dir/', '$datadir/', info, '$system');
TransformDatahb('$date', '$mouse', suffix);
exit;
END

    echo "Done"
    echo
fi

echo "Finished running OIS processing"
