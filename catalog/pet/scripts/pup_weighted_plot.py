from __future__ import division
import sys, os, argparse
import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

versionNumber='2'
dateString='20140320_1212'
author='flavin'
progName=os.path.split(sys.argv[0])[-1]
idstring = '$Id: %s,v %s %s %s Exp $'%(progName,versionNumber,dateString,author)

axesBgColor = '#f8f9fe'
figBgColor = '#eeeeee'
axScale = [1.1,.98] # [width,height]
axMargin = [-.01,.04] # [left,bottom]
lineProperties = {'linestyle':'-','linewidth':1.5}
legendProperties = {'fontsize':'large', 'bbox_to_anchor':(0,-.2,1,1), 'loc':8, 'handlelength':.1, 'columnspacing':1, 'handletextpad':.5}
figFileProperties = {'dpi':160, 'facecolor':figBgColor}
xlabel = 'Time (s)'
ylabel = 'Combined Region Activity'


def main():
    #######################################################
    # PARSE INPUT ARGS
    parser = argparse.ArgumentParser(description='Generate timecourse image from PUP .tac files')
    parser.add_argument('-v', '--version',
                        help='Print version number and exit',
                        action='version',
                        version=versionNumber)
    parser.add_argument('--idstring',
                        help='Print id string and exit',
                        action='version',
                        version=idstring)
    parser.add_argument('-s','--suffix',
                        default='None',
                        help='Suffix to append to ROI label in tac file names, e.g. \'_RSF\'. If option is not given or is "None", no string will be inserted between the roi label and \'.tac\' in the filenames.')
    parser.add_argument('-p','--prefix',
                        default='None',
                        help='Prefix to all tac file names. If option is not given or is "None", sessionLabel will be used.')
    parser.add_argument('sessionLabel',
                        help='PET session label')
    parser.add_argument('config',
                        help='Config file. Each line is "name color tacFileLabelCSV" for all ROIs.')
    parser.add_argument('indir',
                        help='Directory with tac files')
    parser.add_argument('outdir',
                        help='Path to output directory')
    args=parser.parse_args()

    tacExtension = '.tac' if args.suffix == 'None' else args.suffix+'.tac'
    tacPrefix = args.sessionLabel if args.prefix == 'None' else args.prefix
    outputImg = os.path.join(args.outdir,args.sessionLabel+'_roi_activity_plot.png')
    indir = args.indir
    config = args.config
    sessionLabel = args.sessionLabel
    #######################################################

    #######################################################
    # GET ROI NAMES, FILES, AND COLORS FROM CONFIG FILE
    roiFiles,roiColors = parseConfigFile(config)
    #######################################################

    #######################################################
    # READ TAC FILES, BUILD ARRAYS TO PLOT
    toPlotList = []
    labels = []
    for roiName,tacFileLabelCSV in roiFiles.iteritems():
        print 'Getting data for ROI '+roiName
        roiColor = roiColors[roiName]

        # Get tac filenames from csv suffixes
        tacFileList = [os.path.join(indir,tacPrefix+'_'+tacLabel+tacExtension) for tacLabel in tacFileLabelCSV.split(',')]

        # Get data to plot from tac file(s)
        toPlotTimes,toPlotMeans = getPlotData(tacFileList)

        # Add data to plot list, and label to label list
        toPlotList.extend((toPlotTimes,toPlotMeans,roiColor))
        labels.append(roiName)
    #######################################################

    #######################################################
    # PLOT

    # Set up the axes. We have to scale and move them to make room for the legend.
    ax = plt.subplot(111,axisbg=axesBgColor)
    box = ax.get_position()
    ax.set_position([box.x0 + axMargin[0],
                    box.y0 + axMargin[1] + box.height * (1 - axScale[1]),
                    box.width * axScale[0],
                    box.height * axScale[1]])

    # Now plot
    plt.plot(*toPlotList,**lineProperties)

    # Add labels, legend, etc.
    plt.grid(b=True)
    plt.axis('tight')
    plt.title(sessionLabel)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    legend = plt.legend( labels, ncol=len(labels), **legendProperties )
    legend.get_frame().set_facecolor(axesBgColor)

    # Save plot to a file
    print 'Writing image '+outputImg
    plt.savefig(outputImg, **figFileProperties)

def parseConfigFile(configFile):
    import re

    print 'Reading config file '+configFile
    with open(configFile,'r') as f:
        lines = f.read().splitlines()

    roiCSV = lines[0].split('=')[1]
    roiList = [roi.strip() for roi in roiCSV.split(',')]

    roiORRe = '(?P<roi>(%s))'%'|'.join(roiList)
    colorRe = re.compile(r'^%s_COLOR=(?P<color>\d{1,3}:\d{1,3}:\d{1,3})$'%roiORRe)
    filenameRe = re.compile(r'^%s_FILES_SUFFIX=(?P<files>.*)$'%roiORRe)

    roiFiles = {}
    roiColors = {}
    for line in lines[1:]:
        cmatch = colorRe.match(line)
        fmatch = filenameRe.match(line)
        if cmatch:
            roiName = cmatch.group('roi')
            roiColor = cmatch.group('color')
            roiColors[roiName] = colorHexString(roiColor)
        elif fmatch:
            roiName = fmatch.group('roi')
            roiFileCSV = fmatch.group('files')
            roiFiles[roiName] = roiFileCSV

    if roiFiles.viewkeys() != roiColors.viewkeys():
        roiDiffStr = '\n'.join('ROI: %s\t  Color: %s\t  tac(s): %s'%(roi,roiColors.get(roi,'None'),roiFiles.get(roi,'None')) for roi in (roiFiles.viewkeys()-roiColors.viewkeys()))
        sys.exit(progName+' ERROR: Config file %s invalid. Must have one color and at least one tac filename for all ROIs.\n%s'%(configFile,roiDiffStr))

    return roiFiles,roiColors

def colorHexString(colorTupleString):
    '''Takes a colon-separated RGB int string with values between 0 and 255.
        Returns a color hex string'''
    return '#{:02x}{:02x}{:02x}'.format(*[int(c) for c in colorTupleString.split(':')])

def getPlotData(tacFileList):
    # Parse input tac files
    numVoxels,mean,startTime,duration = getTacData(tacFileList)

    # Calculate weighted mean if the roi has more than one tac file
    if len(tacFileList)==1:
        toPlotMeans = mean.squeeze()
    else:
        toPlotMeans = (np.dot(numVoxels.transpose(),mean)/sum(numVoxels)).squeeze()

    # Calculate time values
    toPlotTimes = startTime+duration/2

    return toPlotTimes,toPlotMeans

def getTacData(tacFiles):
    numTacs = len(tacFiles)

    # Parse the first tac file.
    numVoxelsInit,meanInit,startTimeInit,durationInit = parseTac(tacFiles[0])

    # Use the size of its lists to initialize the numpy arrays.
    numVoxels = np.zeros([numTacs,1])
    numVoxels[0,0] = numVoxelsInit
    mean = np.zeros([numTacs,len(meanInit)])
    mean[0,:] = meanInit
    startTime = np.array(startTimeInit)
    duration = np.array(durationInit)

    # If there are any additional files, add their mean and numVoxels data to the arrays
    if numTacs > 1:
        for i,tacFile in enumerate(tacFiles[1:]):
            numVoxels[i+1,0],mean[i+1,:] = parseTac(tacFile,readAll=False)

    return numVoxels,mean,startTime,duration

def parseTac(tacFile, readAll=True):
    print 'Reading tac file '+tacFile
    with open(tacFile,'r') as f:
        lines = f.read().splitlines()

    titles = lines[0].split()
    numVoxels = int(titles[-1])

    if readAll:
        startTime = []
        duration = []
    mean = []
    for line in lines[1:]:
        columns = line.split()
        if readAll:
            startTime.append(float(columns[1]))
            duration.append(float(columns[2]))
        mean.append(float(columns[3]))

    if readAll:
        return numVoxels,mean,startTime,duration
    return numVoxels,mean

if __name__ == '__main__':
    print idstring
    main()

