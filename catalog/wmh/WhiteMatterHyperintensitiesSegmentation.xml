<?xml version="1.0" encoding="UTF-8"?>
<Pipeline xmlns="http://nrg.wustl.edu/pipeline" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://nrg.wustl.edu/pipeline ..\..\schema\pipeline.xsd" xmlns:ext="org.nrg.validate.utils.ProvenanceUtils" xmlns:fileUtils="http://www.xnat.org/java/org.nrg.imagingtools.utils.FileUtils">
    <name>White Matter Hyperintensities Segmentation</name>
    <location>wmh</location>
    <description>Uses University of Wisconsin W2MHS open source MATLAB toolbox designed for detecting and quantifying White Matter Hyperintensities in Alzheimers and aging related neurological disorders.</description>
    <resourceRequirements>
        <property name="DRMAA_JobTemplate_JobCategory">wmh_q</property>
    </resourceRequirements>
    <documentation>
        <authors>
            <author>
                <lastname>Flavin</lastname>
                <firstname>John</firstname>
                <contact>
                    <email>jflavin@wustl.edu</email>
                </contact>
            </author>
        </authors>
        <version>1.20160328</version>
        <input-parameters>
            <parameter>
                <name>t1_scan</name>
                <values>
                    <schemalink>xnat:imageSessionData/scans/scan/ID</schemalink>
                </values>
                <description/>
            </parameter>
            <parameter>
                <name>t1_is_dicom</name>
                <values>
                    <csv>true</csv>
                </values>
                <description/>
            </parameter>
            <parameter>
                <name>flair_scan</name>
                <values>
                    <schemalink>xnat:imageSessionData/scans/scan/ID</schemalink>
                </values>
                <description/>
            </parameter>
            <parameter>
                <name>flair_is_dicom</name>
                <values>
                    <csv>true</csv>
                </values>
                <description/>
            </parameter>
        </input-parameters>
    </documentation>
    <outputFileNamePrefix>^concat(/Pipeline/parameters/parameter[name='logdir']/values/unique/text(),'/',/Pipeline/parameters/parameter[name='label']/values/unique/text())^</outputFileNamePrefix>
    <parameters>
        <parameter>
            <name>workdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='builddir']/values/unique/text(),'/',/Pipeline/parameters/parameter[name='label']/values/unique/text())^</unique>
            </values>
        </parameter>
        <parameter>
            <name>logdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/LOGS')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>datadir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/DATA')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>procdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/PROC')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>tempdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/TEMP')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>t1dir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='datadir']/values/unique/text(), '/', /Pipeline/parameters/parameter[name='t1_scan']/values/unique/text())^</unique>
            </values>
        </parameter>
        <parameter>
            <name>flairdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='datadir']/values/unique/text(), '/', /Pipeline/parameters/parameter[name='flair_scan']/values/unique/text())^</unique>
            </values>
        </parameter>
        <parameter>
            <name>datestamp</name>
            <values>
                <unique>^if(boolean(/Pipeline/parameters/parameter[name='use_datestamp'])) then /Pipeline/parameters/parameter[name='use_datestamp']/values/unique/text() else concat(translate(ext:GetDate(),'-',''),translate(ext:GetTime(),':',''))^</unique>
            </values>
        </parameter>
        <parameter>
            <name>flair_session_resolved</name>
            <values>
                <unique>^if(boolean(/Pipeline/parameters/parameter[name='flair_session'])) then /Pipeline/parameters/parameter[name='flair_session']/values/unique/text() else /Pipeline/parameters/parameter[name='id']/values/unique/text()^</unique>
            </values>
        </parameter>
        <parameter>
            <name>t1_resource</name>
            <values>
                <unique>^if(/Pipeline/parameters/parameter[name='t1_is_dicom']/values/unique/text()='true') then 'DICOM' else 'NIFTI'^</unique>
            </values>
        </parameter>
        <parameter>
            <name>flair_resource</name>
            <values>
                <unique>^if(/Pipeline/parameters/parameter[name='flair_is_dicom']/values/unique/text()='true') then 'DICOM' else 'NIFTI'^</unique>
            </values>
        </parameter>
        <parameter>
            <name>assessorId</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='id']/values/unique/text(),'_wmh_',/Pipeline/parameters/parameter[name='datestamp']/values/unique/text())^</unique>
            </values>
        </parameter>
        <parameter>
            <name>assessorLabel</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='label']/values/unique/text(),'_wmh_',/Pipeline/parameters/parameter[name='datestamp']/values/unique/text())^</unique>
            </values>
        </parameter>
        <parameter>
            <name>assessorXMLPath</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='tempdir']/values/unique/text(),'/assessor.xml')^</unique>
            </values>
        </parameter>
    </parameters>
    <steps>
        <!-- <step id="VALIDATE" description="Check that we have scans to run on" precondition="^if(/Pipeline/parameters/parameter[name='hasFcScans']/values/unique/text()) then /Pipeline/parameters/parameter[name='hasStimScans']/values/unique/text() else false()^">
            <resource name="Fail" location="pipeline-tools">
                <argument id="errstring">
                    <value>Pipeline failed.</value>
                </argument>
                <argument id="errstring">
                    <value>I could not find any "fc" or "stim" scans.</value>
                </argument>
            </resource>
        </step> -->
        <step id="MKDIR" description="Make directories">
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^/Pipeline/parameters/parameter[name='tempdir']/values/unique/text()^</value>
                </argument>
            </resource>
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^/Pipeline/parameters/parameter[name='procdir']/values/unique/text()^</value>
                </argument>
            </resource>
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^/Pipeline/parameters/parameter[name='t1dir']/values/unique/text()^</value>
                </argument>
            </resource>
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^/Pipeline/parameters/parameter[name='flairdir']/values/unique/text()^</value>
                </argument>
            </resource>
        </step>
        <step id="GET_T1" description="Download T1 scan" workdirectory="^/Pipeline/parameters/parameter[name='t1dir']/values/unique/text()^">
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="sessionId">
                    <value>^fileUtils:getJSESSION('DUMMY')^</value>
                </argument>
                <argument id="absolutePath"/>
                <argument id="batch" />
                <argument id="method">
                    <value>GET</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'/scans/',/Pipeline/parameters/parameter[name='t1_scan']/values/unique/text(),'/resources/',/Pipeline/parameters/parameter[name='t1_resource']/values/unique/text(),'/files"')^</value>
                </argument>
            </resource>
        </step>
        <step id="GET_FLAIR" description="Download FLAIR scan" workdirectory="^/Pipeline/parameters/parameter[name='flairdir']/values/unique/text()^">
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="sessionId">
                    <value>^fileUtils:getJSESSION('DUMMY')^</value>
                </argument>
                <argument id="absolutePath"/>
                <argument id="batch" />
                <argument id="method">
                    <value>GET</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='flair_session_resolved']/values/unique/text(),'/scans/',/Pipeline/parameters/parameter[name='flair_scan']/values/unique/text(),'/resources/',/Pipeline/parameters/parameter[name='flair_resource']/values/unique/text(),'/files"')^</value>
                </argument>
            </resource>
        </step>
        <step id="PARAMS_FILE" description="Create bash params file from pipeline params" >
            <resource name="ParamsToCshParams" location="xnat_tools/resources">
                <argument id="outfile">
                    <value>^concat(/Pipeline/parameters/parameter[name='datadir']/values/unique/text(),'/',/Pipeline/parameters/parameter[name='label']/values/unique/text(),'.params')^</value>
                </argument>
                <argument id="skip">
                    <value>pwd,host,user,u</value>
                </argument>
                <argument id="script">
                    <value>/data/CNDA/pipeline/catalog/pipeline-tools/resources/ParamsToBashParams.xsl</value>
                </argument>
            </resource>
        </step>
        <step id="PROC" description="Run WMH processing" workdirectory="^/Pipeline/parameters/parameter[name='procdir']/values/unique/text()^">
            <resource name="wmhprocessing" location="wmh/resources">
                <argument id="paramsfile">
                    <value>^concat(/Pipeline/parameters/parameter[name='datadir']/values/unique/text(),'/',/Pipeline/parameters/parameter[name='label']/values/unique/text(),'.params')^</value>
                </argument>
            </resource>
        </step>
        <step id="MV_OUTPUT" description="Move some output files that get written in with the raw data">
            <resource name="mv" location="commandlineTools">
                <argument id="source">
                    <value>^concat(/Pipeline/parameters/parameter[name='datadir']/values/unique/text(),'/std/',/Pipeline/parameters/parameter[name='t1_scan']/values/unique/text(),'/*.nii.gz')^</value>
                </argument>
                <argument id="destination">
                    <value>^/Pipeline/parameters/parameter[name='procdir']/values/unique/text()^</value>
                </argument>
            </resource>
            <resource name="mv" location="commandlineTools">
                <argument id="source">
                    <value>^concat(/Pipeline/parameters/parameter[name='datadir']/values/unique/text(),'/std/',/Pipeline/parameters/parameter[name='flair_scan']/values/unique/text(),'/*.nii.gz')^</value>
                </argument>
                <argument id="destination">
                    <value>^/Pipeline/parameters/parameter[name='procdir']/values/unique/text()^</value>
                </argument>
            </resource>
        </step>
        <step id="CLEANUP_1" description="Clean up raw data">
            <resource name="rm" location="commandlineTools">
                <argument id="file">
                    <value>^/Pipeline/parameters/parameter[name='datadir']/values/unique/text()^</value>
                </argument>
                <argument id="r"/>
                <argument id="f"/>
            </resource>
        </step>
        <step id="PUT_ASSESSOR" description="Upload OIS proc assessor XML" workdirectory="^/Pipeline/parameters/parameter[name='procdir']/values/unique/text()^">
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="user">
                    <value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
                </argument>
                <argument id="password">
                    <value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
                </argument>
                <argument id="method">
                    <value>PUT</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'/assessors/',/Pipeline/parameters/parameter[name='assessorId']/values/unique/text(),'?req_format=xml"')^</value>
                </argument>
                <argument id="infile">
                    <value>^concat(/Pipeline/parameters/parameter[name='tempdir']/values/unique/text(),'/assessor.xml')^</value>
                </argument>
            </resource>
        </step>
        <step id="PUT_DATA" description="Upload data as assessor resource">
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="user">
                    <value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
                </argument>
                <argument id="password">
                    <value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
                </argument>
                <argument id="method">
                    <value>PUT</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'/assessors/',/Pipeline/parameters/parameter[name='assessorId']/values/unique/text(),'/resources/DATA/files?overwrite=true&amp;label=DATA&amp;reference=',/Pipeline/parameters/parameter[name='procdir']/values/unique/text(),'"')^</value>
                </argument>
            </resource>
        </step>
        <step id="CLEANUP_2" description="Clean up processed data">
            <resource name="rm" location="commandlineTools">
                <argument id="file">
                    <value>^/Pipeline/parameters/parameter[name='procdir']/values/unique/text()^</value>
                </argument>
                <argument id="r"/>
                <argument id="f"/>
            </resource>
            <resource name="rm" location="commandlineTools">
                <argument id="file">
                    <value>^/Pipeline/parameters/parameter[name='tempdir']/values/unique/text()^</value>
                </argument>
                <argument id="r"/>
                <argument id="f"/>
            </resource>
        </step>
        <step id="PUT_LOGS" description="Upload LOG files">
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="user">
                    <value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
                </argument>
                <argument id="password">
                    <value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
                </argument>
                <argument id="method">
                    <value>PUT</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'/assessors/',/Pipeline/parameters/parameter[name='assessorId']/values/unique/text(),'/resources/LOG/files?overwrite=true&amp;label=LOG&amp;reference=',/Pipeline/parameters/parameter[name='logdir']/values/unique/text(),'"')^</value>
                </argument>
            </resource>
        </step>
    </steps>
</Pipeline>
