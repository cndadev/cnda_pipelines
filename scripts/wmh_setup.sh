#!/bin/bash

export LSTPATH=/nrgpackages/tools.release/spm8_r6313/toolbox/LST
export SPMPATH=/nrgpackages/tools.release/spm8_r6313
export MATLABPATH=/data/CNDA/pipeline/catalog/wmh/resources/matlab
export SCRIPTPATH=/data/CNDA/pipeline/catalog/wmh/scripts

source /data/CNDA/pipeline/scripts/fsl5_setup.sh
source /data/CNDA/pipeline/scripts/epd-python_setup.sh
source /data/CNDA/pipeline/scripts/dcm2niix_setup.sh
# export MATLABROOT=/usr/local/MATLAB/R2015a
